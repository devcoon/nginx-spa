#!/bin/bash
# $Id: entrypoint.sh,v 0.1 2016/05/09 $ $Author: Fabrizio Galiano $

if [ "$1" = "start-nginx" ]; then

    #bootloading for configuration
    /bin/bash /docker/scripts/bootloader.sh

    #start nginx
    nginx -g "daemon off;"

fi