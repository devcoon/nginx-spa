#!/bin/env bash
# $Id: bootloader.sh,v 0.1 2016/05/09 $ $Author: Fabrizio Galiano $

#create group if not exists
#getent group $APP_GROUP || groupadd $APP_GROUP

#create application user if not exists and assig it to the specified application group
#id -u $APP_USER &>/dev/null || useradd -s /bin/bash $APP_USER -g && usermod -a -G $APP_USER $APP_GROUP

#create application folder if not exists
if ! [[ -d $APP_CWD ]] ; then mkdir -p $APP_CWD ; fi

#assign user and group permission to application folder
#chown $APP_USER.$APP_GROUP $APP_CWD

#add vhost configuration template
/bin/cp /docker/configuration/nginx/default.conf /etc/nginx/conf.d/default.conf

for var in $(printenv); do

    #explode vars to retrive key/value pairs
    IFS='=' read -r -a array <<< $var

    export KEY=${array[0]}

    if [[ $KEY =~ VHOST_ ]]; then

        export VALUE=${array[1]}

        sed -i -e 's|<'$KEY'>|'$VALUE'|g' '/etc/nginx/conf.d/default.conf'

    fi

done